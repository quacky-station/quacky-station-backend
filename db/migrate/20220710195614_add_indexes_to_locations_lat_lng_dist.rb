class AddIndexesToLocationsLatLngDist < ActiveRecord::Migration[7.0]
  def change
    add_index :locations, :lat
    add_index :locations, :lng
    add_index :locations, :dist
  end
end
