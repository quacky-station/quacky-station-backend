# frozen_string_literal: true

class Location < ApplicationRecord
  belongs_to :quack

  validates :lat, presence: true
  validates :lng, presence: true
  validates :dist, presence: true
end
