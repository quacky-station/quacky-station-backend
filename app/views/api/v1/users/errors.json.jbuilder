# frozen_string_literal: true

json.errors do
  json.array! @user.errors.full_messages
end
