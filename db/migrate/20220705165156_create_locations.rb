class CreateLocations < ActiveRecord::Migration[7.0]
  def change
    create_table :locations do |t|
      t.references :quack, null: false, foreign_key: true
      t.float :lat
      t.float :lng
      t.integer :dist

      t.timestamps
    end
  end
end
