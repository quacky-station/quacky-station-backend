class ChencheQuacksBodyType < ActiveRecord::Migration[7.0]
  def up
    change_column :quacks, :body, :text
  end

  def down
    change_column :quacks, :body, :string
  end
end
