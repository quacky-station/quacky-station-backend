# frozen_string_literal: true

module Api
  module V1
    class AuthController < ApplicationController
      skip_before_action :authenticate_request

      def login
        @user = User.find_by email: params[:email]
        if @user&.authenticate(params[:password])
          @token = jwt_encode user_id: @user.id
          render :login, status: :ok
        else
          render :errors, status: :unauthorized
        end
      end
    end
  end
end
