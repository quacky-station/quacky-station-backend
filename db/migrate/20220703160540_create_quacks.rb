class CreateQuacks < ActiveRecord::Migration[7.0]
  def change
    create_table :quacks do |t|
      t.references :user, null: false, foreign_key: true
      t.string :body, null: false
      t.string :image

      t.timestamps
    end
  end
end
