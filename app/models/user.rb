# frozen_string_literal: true

class User < ApplicationRecord
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i

  has_many :quacks, dependent: :destroy

  has_secure_password

  before_validation :downcase_email

  validates :username, presence: true, length: { minimum: 4, maximum: 20 }, uniqueness: true
  validates :email, presence: true, length: { maximum: 255 }, format: { with: VALID_EMAIL_REGEX },
    uniqueness: true
  validates :password_digest, presence: true, length: { minimum: 6 }, allow_nil: true

  def downcase_email
    self.email = email.downcase
  end
end
