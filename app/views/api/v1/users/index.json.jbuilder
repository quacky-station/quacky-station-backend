# frozen_string_literal: true

json.users do
  json.array! @users do |user|
    json.id user.id
    json.username user.username
  end
end
