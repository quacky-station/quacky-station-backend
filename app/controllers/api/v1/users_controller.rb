# frozen_string_literal: true

module Api
  module V1
    class UsersController < ApplicationController
      skip_before_action :authenticate_request, only: %i[create]
      before_action :set_user, only: %i[show update destroy]

      def index
        @users = User.all
        render :index, status: :ok
      end

      def show
        render :show, status: :found
      end

      def create
        @user = User.new user_params
        if @user.save
          render :create, status: :created
        else
          render :errors, status: :unprocessable_entity
        end
      end

      def update
        if @user.update user_params
          render :show
        else
          render :errors, status: :unprocessable_entity
        end
      end

      def destroy
        @user.destroy
      end

      private

      def user_params
        params.require(:user).permit(:username, :email, :password, :password_confirmation)
      end

      def set_user
        @user = User.find(params[:id])
      end
    end
  end
end
