# frozen_string_literal: true

class Quack < ApplicationRecord
  belongs_to :user
  has_one :location, dependent: :destroy

  validates :body, presence: true
end
